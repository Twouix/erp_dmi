let express = require('express');
let mustacheExpress = require('mustache-express');
let bodyParser = require('body-parser');

let app = express();

const port = 3456

app.set('views', `${__dirname}/src/views`);
app.set('view engine', 'mustache');
app.engine('mustache', mustacheExpress());
app.use (bodyParser.urlencoded({ extended : false }));
app.use(bodyParser.json({ type:'application/json' }));
app.use(express.static('public'));

//Controllers
let home_controller = require('./src/controllers/homeController');
let dmi_controller = require('./src/controllers/dmiController');
let users_controller = require('./src/controllers/usersController');

//Routes
app.get('/', home_controller.index);
app.post('/medical-exam', dmi_controller.newExam);
app.post('/history', dmi_controller.filtre);
app.get('/history', dmi_controller.index);
app.get('/users', users_controller.index);
app.post('/patients', users_controller.add_user);
app.get('/new-patient', users_controller.form_user);
app.get("/patients/:id", users_controller.get_user);

//default route => home
app.get('*', (req, res) => {
    res.redirect('/');
});

//Server start
app.listen(port, () => {
    console.log("Server started  listening at http://localhost:"+port);
});
