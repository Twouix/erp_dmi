const parser = require('csv-parser')
const fs = require('fs')
// path to our patient csv file
const PATIENTS_CSV = "src/assets/patients.csv";

const createCsvWriter = require('csv-writer').createObjectCsvWriter;
// creating object to write in csv
const csvWriter = createCsvWriter({
    path: 'src/assets/patients.csv',
    // we'll write our patient's info in this order
    header: [{
            id: 'matricule',
            title: 'ID_Patient'
        },
        {
            id: 'nom',
            title: 'Nom'
        },
        {
            id: 'prenom',
            title: 'Prenom'
        },
        {
            id: 'naissance',
            title: 'Date_Naissance'
        },
        {
            id: 'mutuelle',
            title: 'ID_Mutuelle'
        }
    ],
    append: true
});

exports.index = function(req, res) {
    const p_data= [];

    fs.createReadStream('src/assets/patients.csv')
    .pipe(parser())
    .on('data', (data) => p_data.push(data))
    .on('end',() => res.render('users',{
        liste : p_data
    }));
};

exports.form_user = function (req, res) {
    res.render('add_user');
};

exports.add_user = function (req, res) {
    // get patient info
    const records = [{
        matricule: req.body.matricule,
        nom: req.body.nom,
        prenom: req.body.prenom,
        naissance: req.body.naissance,
        mutuelle: req.body.mutuelle
    }];

    // read known patients file
    let registeredPatients = [];

    fs.createReadStream(PATIENTS_CSV)
        .pipe(parser())
        .on("data", (data) => registeredPatients.push(data))
        .on("end", function () {

            // find our patient
            for (p of registeredPatients) {
                if (p.ID_Patient == records[0].matricule) {
                    // patient found: ignore
                    res.redirect('/new-patient');
                    return;
                }
            }

            // patient not found : create it
            // writing into the csv file
            csvWriter.writeRecords(records)
                .then(() => {
                    res.redirect('/new-patient');
                }).catch((err) => {
                    console.log("================");
                    console.log('Error :');
                    console.log(err);
                    console.log("================");
                    res.redirect('/new-patient');
                });
        });
};

/**
 * @brief Get an existing patient from patients csv file
 * @param req Http GET request with last path parameter being the patient ID (e.g. patients/{id})
 * @return HttpResponse : 200 if OK (returns JSON), 400 if request params are incorrect, 404 if patient no found, 500 if error
 */
exports.get_user = function (req, res) {

    // get patient ID
    let patientId = req.params.id;

    if (patientId == undefined || patientId == "") {
        res.sendStatus(400);
        return;
    }

    // read known patients file
    let registeredPatients = [];

    fs.createReadStream(PATIENTS_CSV)
        .pipe(parser())
        .on("data", (data) => registeredPatients.push(data))
        .on("end", function () {

            // find our patient
            for (p of registeredPatients) {
                if (p.ID_Patient == patientId) {
                    // patient found, changing fields name to correspond to spec
                    p.ID_PATIENT = p.ID_Patient;
                    p.ID_MUTUELLE = p.ID_Mutuelle;
                    delete p.ID_Patient;
                    delete p.ID_Mutuelle;
                    res.json(p);

                    return;
                }
            }

            // patient not found
            res.sendStatus(404);
        });
};