const csv = require('csv-parser');
const fs = require('fs');
const createCsvStringifier = require('csv-writer').createObjectCsvStringifier;
const createCsvWriter = require('csv-writer').createArrayCsvWriter;

/**
 * @brief Add a new exam in the dmi file
 * @param req Http POST request containing :
 *                      ID_MUTUELLE (string) : the 'mutuelle' of the patient
 *                      ID_PATIENT (string) : the patient's id who's taken the exam
 *                      date (string) : the exam's date
 *                      intervention (string) : the exam's name
 *                      commentaire (string) : the exam's comment
 *                      lieu (string) : place where exam took place
 * @return HttpResponse : 200 if OK, 400 if request params are incorrect, 500 if error
 */
exports.newExam = function (req, res) {
    //Checking params
    const requiredParams = ["ID_MUTUELLE", "ID_PATIENT", "date", "intervention", "commentaire", "lieu"];
    for(let i = 0; i<requiredParams.length; i++){
        if(req.body[requiredParams[i]] == undefined || req.body[requiredParams[i]] == ""){
            res.sendStatus(400);//Bad request
            return;
        }
    }

    //Initiating writer
    const csvWriter = createCsvWriter({
        header: ["Date","Patient","Intervention","Commentaire","Lieu","Mutuelle"],
        append: true,
        path: 'src/assets/dmi.csv'
    });

    //Creating lines to add (contains only one line)
    let linesToAdd = [];
    linesToAdd.push([
        req.body.date,
        req.body.ID_PATIENT,
        req.body.intervention,
        req.body.commentaire,
        req.body.lieu,
        req.body.ID_MUTUELLE
    ]);

    //Writing
    csvWriter.writeRecords(linesToAdd).then(() => {
        res.sendStatus(200);//Ok
        return;
    }).catch( (err) => {
        console.log(err)
        res.sendStatus(500);//Internal server error
        return;
    });
};

//get render for /history
exports.index = function(req, res) {

    let h_data= []; 

    //reading csv
    fs.createReadStream('src/assets/dmi.csv')
    .pipe(csv())
    .on('data', (data) => h_data.push(data))    //collecting data
    .on('end',() => res.render('history',{      //rendering result
        liste : h_data
    })
    );
};

//post render for /history (with filters)
exports.filtre = function(req, res) {

    let h_data= [];
    let data_filtre="";

    //reading csv
    fs.createReadStream('src/assets/dmi.csv')
    .pipe(csv())
    .on('data', (data) => {

        if(req.body["patient"] != ""){              //existing filter ?
            data_filtre = req.body["patient"];      //collect filter infomramtion to display
            if(req.body.patient == data['Patient']) //testing if Patient ID from row == Patient Id from filter
                h_data.push(data);                  //collecting data
        }else{                                      //else : gathering all data
            h_data.push(data);
        }

    }).on('end',() => res.render('history',{        //rendering final result
        liste : h_data,
        filtre : data_filtre
    })
    );
};
