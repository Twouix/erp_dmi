# Spécification technique des échanges de données

## Accès de l'interface

Notre groupe est censé pouvoir répondre à deux points:

- Transmettre les informations du patient, à l'attention de l'hopital, au groupe 4.
- Recevoir les informations des examens pratiqués du groupe 4.

### Transmission Informations patients

La receptionner de la demande d'envoi de ces données pour un patient précis se fait grâce à son identifiant et par la route: "/patients/:id".

Cette route est accessible à tout moment en GET pour le groupe 4.

### Reception des Examens

La reception des examens pratiqués sur un patient ce fait par la route: "/medical-exam".

Cette route est accessible à tout moment en POST pour le groupe 4 (cf. filetodo pour un exemple de données récéptionnées).

## Stratégie d'échange

Comme convenu avec tous les groupes en début de projet, toutes les données envoyés, et donc reçus, sont en format JSON dans le body des requêtes.
